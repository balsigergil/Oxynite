﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class GameManagerTest {

	[Test]
	public void GameManager_RegisterNewPlayer() {
        GameManager.players.Clear();
        Object playerPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Player.prefab", typeof(Player));
        Player player1 = PrefabUtility.InstantiatePrefab(playerPrefab) as Player;
        Player player2 = PrefabUtility.InstantiatePrefab(playerPrefab) as Player;
        GameManager.RegisterPlayer("test01", player1);
        GameManager.RegisterPlayer("test02", player2);
        Assert.AreEqual(GameManager.players.Count, 2);
    }

    [Test]
    public void GameManager_UnRegisterNewPlayer()
    {
        GameManager.players.Clear();
        Object playerPrefab = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Player.prefab", typeof(Player));
        Player player1 = PrefabUtility.InstantiatePrefab(playerPrefab) as Player;
        Player player2 = PrefabUtility.InstantiatePrefab(playerPrefab) as Player;
        GameManager.RegisterPlayer("test01", player1);
        GameManager.RegisterPlayer("test02", player2);
        GameManager.UnregisterPlayer("Player test01");
        Assert.AreEqual(GameManager.players.Count, 1);
    }
}
