﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Weapon graphics properties
/// </summary>
public class WeaponGraphics : MonoBehaviour {

    public ParticleSystem muzzleFlash;
    public GameObject hitEffectPrefab;
    
}

